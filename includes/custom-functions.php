<?php

if ( ! function_exists( 'excellence_custom_footer' ) ) {

    /**
     * 
     * Print footer with 3 columns
     * 
     * @author Everaldo Matias - https://everaldo.dev
     * 
     * @since 03/04/2020
     * @version 1.0 - 03/04/2020
     * 
     */

    function excellence_custom_footer() {

        echo '<div class="col">';

            echo '<h2>Uma ação Diginet Brasil</h2>';
            echo '<p>A Diginet Brasil é a empresa admirada pelos seus clientes e usuários. Esse reconhecimento é resultado de anos de atividades sempre com foco no crescimento da nossa economia, na excelência de serviços prestados e no respeito ao cidadão.</p>';

        echo '</div><!-- /.col -->';

        echo '<div class="col">';
            excellence_footer_menu();
        echo '</div><!-- /.col -->';

        echo '<div class="col">';
            the_social_icons();
            echo '<h2 class="site-title">' . esc_html( get_bloginfo( 'name' ) ) . '</h2>';
            excellence_address_infos();
            excellence_contact_infos();
        echo '</div><!-- /.col -->';

    }

}

remove_action( 'excellence_footer', 'excellence_footer_3_columns', 10  );
add_action( 'excellence_footer', 'excellence_custom_footer', 10 );

if ( ! function_exists( 'excellence_get_excerpt' ) ) {

    /**
     * 
     * Function excellence_get_excerpt
     * 
     * @author Everaldo Matias - https://everaldo.dev
     * 
     * @since   19/05/2020
     * @version 1.0 - 19/05/2020
     *
     * @param  string $content with text to excerpt.
     * @param  string $limit number of the limit.
     * @param  string $after with element to print in end excerpt.
     *
     * @return string
     * 
     */

    function excellence_get_excerpt( $content = '', $limit = '', $after = '' ) {
        
        if ( $limit ) {
            $l = $limit;
        } else {
            $l = '140';
        }

        if ( $content ) {
            $excerpt = $content;
        } else {
            $excerpt = get_the_content();
        }
    
        $excerpt = preg_replace( " (\[.*?\])",'',$excerpt );
        $excerpt = strip_shortcodes( $excerpt );
        $excerpt = strip_tags( $excerpt );
        $excerpt = substr( $excerpt, 0, $l );
        $excerpt = substr( $excerpt, 0, strripos($excerpt, " " ) );
        $excerpt = trim( preg_replace( '/\s+/', ' ', $excerpt ) );
        
        if ( $after ) {
            $a = $after;
        } else {
            $a = '...';
        }

        $excerpt = $excerpt . $a;
        return $excerpt;

    }

}

/**
 * Specific templates for CPT Profile
 */
function excellence_single_template( $single_template ) {
    
    global $post;
    
	if ( 'perfil' == $post->post_type ) {
        $single_template = EXCELLENCE_PLUGIN_TEMPLATE_DIR . '/single-profile.php';
    }
    
    return $single_template;
    
}
add_filter( 'single_template', 'excellence_single_template' );

function excellence_archive_templates( $archive_template ) {
    
    global $post;
    
    if ( is_tax( 'cidade' ) || is_tax( 'atuacao' ) ) {
        $archive_template = EXCELLENCE_PLUGIN_TEMPLATE_DIR . '/taxonomy.php';
    }
    
    return $archive_template;
    
}
add_filter( 'archive_template', 'excellence_archive_templates' );

function excellence_custom_body_class( $classes ) {

    if ( is_tax( 'cidade' ) || is_tax( 'atuacao' ) ) {
        $classes[] = 'search-results';
    }
    
    return $classes;

}

add_filter( 'body_class', 'excellence_custom_body_class' );

if ( ! function_exists( 'excellence_get_term_by_slug' ) ) {

    function excellence_get_term_by_slug( $term, $taxonomy ) {
        
        $term = get_term_by( 'slug', $term, $taxonomy );

        if ( $term && ! is_wp_error( $term ) ) {

            return esc_html( $term->name );

        }    

    }

}