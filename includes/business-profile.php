<?php

/**
 * 
 * [1] excellence_post_type_perfil
 * [2] excellence_taxonomies_profile
 * [3] excellence_set_perfil_terms
 * [4] Functions to Post My CF7 Form
 * [4.1] excellence_update_terms_on_create_perfil
 * [4.2] excellence_filter_perfil_ip
 * [4.3] excellence_filter_perfil_horario
 * [4.4] excellence_filter_perfil_data
 * [5] excellence_get_register_section
 * [6] excellence_register_business_form
 * [7] excellence_shortcode_business_form
 * [8] excellence_section_links
 * [9] excellence_get_section_search_business
 * [10] excellence_include_templates
 * 
 */

if ( class_exists( 'Excellence_Post_Type' ) ) {

    /**
     * 
     * [1] excellence_post_type_perfil
     * 
     */
    function excellence_post_type_perfil() {

        $profile = new Excellence_Post_Type(
            'Perfil de Negócios',
            'perfil'
        );

        $profile->set_labels(
            [
                'name'         => __( 'Perfis de Negócios', 'excellence' ),
                'menu_name'    => __( 'Perfil de Negócios', 'excellence' ),
                'all_items'    => __( 'Todos perfis', 'excellence' ),
                'add_new'      => __( 'Adicionar perfil', 'excellence' ),
                'not_found'    => __( 'Nenhum perfil encontrado. <a href                  ="' . esc_url( admin_url() ) . '/post-new.php?post_type=profile">Clique aqui para criar o primeiro Perfil de Negócios</a>', 'excellence' ),
                'add_new_item' => __( 'Adicionar novo Perfil de Negócios', 'excellence' ),
            ]
        );
        $profile->set_arguments(
            [
                'supports' => [ 'title', 'editor', 'thumbnail' ],
                'menu_icon' => 'dashicons-id-alt'
            ]
        );

    }

    add_action( 'init', 'excellence_post_type_perfil', 1 );

}

if ( class_exists( 'Excellence_Taxonomy' ) ) {

    /**
     * 
     * [2] excellence_taxonomies_profile
     * 
     */
    function excellence_taxonomies_profile() {

        $occupation = new Excellence_Taxonomy(
            'Área de Atuação', // Nome (Singular) da nova Taxonomia.
            'atuacao', // Slug do Taxonomia.
            'perfil' // Nome do tipo de conteúdo que a taxonomia irá fazer parte.
        );

        $occupation->set_labels(
            array(
                'menu_name' => __( 'Áreas de Atuação', 'excellence' )
            )
        );

        $occupation->set_arguments(
            array(
                'hierarchical' => false
            )
        );

        $zip = new Excellence_Taxonomy(
            'CEP', // Nome (Singular) da nova Taxonomia.
            'cep', // Slug do Taxonomia.
            'perfil' // Nome do tipo de conteúdo que a taxonomia irá fazer parte.
        );

        $zip->set_labels(
            array(
                'menu_name' => __( 'CEPs', 'excellence' )
            )
        );

        $zip->set_arguments(
            array(
                'hierarchical' => false
            )
        ); 
        
        // District
        $district = new Excellence_Taxonomy(
            'Bairro', // Nome (Singular) da nova Taxonomia.
            'bairro', // Slug do Taxonomia.
            'perfil' // Nome do tipo de conteúdo que a taxonomia irá fazer parte.
        );

        $district->set_labels(
            array(
                'menu_name' => __( 'Bairros', 'excellence' )
            )
        );

        $district->set_arguments(
            array(
                'hierarchical' => false
            )
        ); 

        // City
        $city = new Excellence_Taxonomy(
            'Cidade', // Nome (Singular) da nova Taxonomia.
            'cidade', // Slug do Taxonomia.
            'perfil' // Nome do tipo de conteúdo que a taxonomia irá fazer parte.
        );

        $city->set_labels(
            array(
                'menu_name' => __( 'Cidades', 'excellence' )
            )
        );

        $city->set_arguments(
            array(
                'hierarchical' => false
            )
        );

        // UF
        $uf = new Excellence_Taxonomy(
            'Estado', // Nome (Singular) da nova Taxonomia.
            'uf', // Slug do Taxonomia.
            'perfil' // Nome do tipo de conteúdo que a taxonomia irá fazer parte.
        );

        $uf->set_labels(
            array(
                'menu_name' => __( 'Estados', 'excellence' )
            )
        );

        $uf->set_arguments(
            array(
                'hierarchical' => false
            )
        );

    }

    add_action( 'init', 'excellence_taxonomies_profile', 1 );

}

/**
 * 
 * [3] excellence_set_perfil_terms
 * 
 * New wrapper to wp_set_post_terms function
 * 
 */
function excellence_set_perfil_terms( $post_id, $value, $taxonomy ) {

    // Determines whether a taxonomy term exists.
    $term = term_exists( $value, $taxonomy );

	// Creates the term does not yet exist.
	if ( 0 === $term || null === $term ) {

		$term = wp_insert_term(
			$value,
			$taxonomy,
			array(
				'slug' => strtolower( str_ireplace( ' ', '-', $value ) )
			)
		);

	} else {
        
        $term_object = get_term_by( 'id', absint( $term['term_id'] ), $taxonomy );
        $term = $term_object->name;

    }

	// Define the taxonomy term.
	wp_set_post_terms( $post_id, $term, $taxonomy, false );

}

/**
 * 
 * [4] Functions to Post My CF7 Form
 * 
 */

/**
 * 
 * [4.1] excellence_update_terms_on_create_perfil
 * 
 * Function to take further action once form has been submitted and saved as a post.  Note this action is only fired for submission which has been submitted as opposed to saved as drafts.
 * 
 * @param string $post_id new post ID to which submission was saved.
 * @param array $cf7_form_data complete set of data submitted in the form as an array of field-name=>value pairs.
 * @param string $cf7form_key unique key to identify your form.
 * 
 */
function excellence_update_terms_on_create_perfil( $post_id, $cf7_form_data, $cf7form_key ) {

    // Areas of expertise
    $terms = $cf7_form_data['atuacao'];
    if ( ! empty( $terms ) ) {
        excellence_set_perfil_terms( $post_id, $terms, 'atuacao' );
    } else {
        excellence_set_perfil_terms( $post_id, 'Outros', 'atuacao' );
    }
    
    // CEP
    $cep = $cf7_form_data['cep'];
    if ( ! empty( $cep ) ) {
        excellence_set_perfil_terms( $post_id, $cep, 'cep' );
    }

    // District
    $district = $cf7_form_data['bairro'];
    if ( ! empty( $district ) ) {
        excellence_set_perfil_terms( $post_id, $district, 'bairro' );
    } else {
        excellence_set_perfil_terms( $post_id, 'Outros', 'bairro' );
    }

    // City
    $city = $cf7_form_data['cidade'];
    if ( ! empty( $city ) ) {
        excellence_set_perfil_terms( $post_id, $city, 'cidade' );
    }

    // UF
    $uf = $cf7_form_data['uf'];
    if ( ! empty( $uf ) ) {
        excellence_set_perfil_terms( $post_id, $uf, 'uf' );
    }
    
}
add_action( 'cf7_2_post_form_submitted_to_perfil', 'excellence_update_terms_on_create_perfil', 10, 3 );

if ( ! function_exists( 'excellence_filter_perfil_ip' ) ) {

    /**
     * 
     * [4.2] excellence_filter_perfil_ip
     * 
     * Adds IP addres to the registered business with Post My CF7 Form
     * 
     * @version 0.0.1
     * 
     * @see https://wordpress.org/plugins/post-my-contact-form-7/
     * @see https://www.php.net/manual/pt_BR/reserved.variables.server.php
     * @see https://www.php.net/manual/en/filter.filters.flags.php
     * @see https://developer.wordpress.org/reference/functions/add_filter/
     * 
     */
    function excellence_filter_perfil_ip( $value, $post_id, $form_data ) {

        if ( filter_var( $_SERVER["REMOTE_ADDR"], FILTER_VALIDATE_IP ) ) {
            $return = $_SERVER["REMOTE_ADDR"];
        } else {
            $return = 'IP não identificado!';
        }
    
        return $return;

    }

    add_filter( 'cf7_2_post_filter-perfil-ip', 'excellence_filter_perfil_ip', 10 ,3 );

}

if ( ! function_exists( 'excellence_filter_perfil_horario' ) ) {

    /**
     * 
     * [4.3] excellence_filter_perfil_horario
     * 
     * Adds registration time to the registered business with Post My CF7 Form
     * 
     * @version 0.0.1
     * 
     * @see https://www.php.net/manual/pt_BR/class.datetimezone.php
     * @see https://www.php.net/manual/pt_BR/class.datetime.php
     * @see https://developer.wordpress.org/reference/functions/get_option/
     * @see https://developer.wordpress.org/reference/functions/add_filter/
     * 
     */
    function excellence_filter_perfil_horario( $value, $post_id, $form_horario ) {

        $timezone_string = new DateTimeZone( get_option( 'timezone_string' ) );
        $date = new DateTime( 'now', $timezone_string );

        return $date->format( 'H:i:s' );

    }

    add_filter( 'cf7_2_post_filter-perfil-horario', 'excellence_filter_perfil_horario', 10 ,3 );

}

if ( ! function_exists( 'excellence_filter_perfil_data' ) ) {

    /**
     * 
     * [4.4] excellence_filter_perfil_data
     * 
     * Adds registration time to the registered business with Post My CF7 Form
     * 
     * @version 0.0.1
     * 
     * @see https://www.php.net/manual/pt_BR/class.datetimezone.php
     * @see https://www.php.net/manual/pt_BR/class.datetime.php
     * @see https://developer.wordpress.org/reference/functions/get_option/
     * @see https://developer.wordpress.org/reference/functions/add_filter/
     * 
     */
    function excellence_filter_perfil_data( $value, $post_id, $form_data ) {

        $timezone_string = new DateTimeZone( get_option( 'timezone_string' ) );
        $date = new DateTime( 'now', $timezone_string );

        return $date->format( 'd/m/Y' );
    
    }

    add_filter( 'cf7_2_post_filter-perfil-data', 'excellence_filter_perfil_data', 10 ,3 );

}

if ( ! function_exists( 'excellence_get_register_section' ) ) {

    /**
     * 
     * [5] excellence_get_register_section
     * 
     * Function to print complete section on home with register business
     * 
     */
    function excellence_get_register_section() {

        excellence_open_container();
            echo '<div class="col">';
                excellence_register_business_form();
            echo '</div><!-- /.col -->';
        excellence_close_container();

    }

}

if ( ! function_exists( 'excellence_register_business_form' ) ) {

    /**
     * 
     * [6] excellence_register_business_form
     * 
     * Function wrap to print form register business
     * 
     */
    function excellence_register_business_form() {
        echo '<div class="excellence_register_business_form">';
            echo do_shortcode( '[cf7form cf7key="cadastro"]' );
        echo '</div><!-- /.excellence_register_business_form -->';
    }

}

if ( ! function_exists( 'excellence_shortcode_business_form' ) ) {

    /**
     * 
     * [7] excellence_shortcode_business_form
     * 
     * Create shortcode to print form register business
     * 
     */
    function excellence_shortcode_business_form() {
        ob_start();
        excellence_register_business_form();
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
    add_shortcode( 'business_register', 'excellence_shortcode_business_form' );
}

if ( ! function_exists( 'excellence_section_links' ) ) {

    /**
     * 
     * [8] excellence_section_links
     * 
     * Function to print links to register form and how to use
     * 
     */
    function excellence_get_section_links_home() {

        echo '<div class="wrap-links-page-home">';
        
            excellence_open_container();
                echo '<div class="col -text-center">';
                    
                    echo '<img class="icon" src="' . plugin_dir_url( __DIR__ ) . 'assets/images/diginet-icon-register.png">';    
                    echo '<h2 class="title-line">Cadastre seu Negócio!</h2>';
                    echo '<p>Cadastre seu negócio agora mesmo, é rápido e prático. Após aprovação seu negócio estará disponível, o que ajudará em sua divulgação.</p>';
                    echo '<a href="' . esc_url( home_url() . '/cadastro/' ) . '">Cadastre-se!</a>';

                echo '</div><!-- /.col -->';
                echo '<div class="col -text-center">';
                    
                    echo '<img class="icon" src="' . plugin_dir_url( __DIR__ ) . 'assets/images/diginet-icon-faq.png">';
                    echo '<h2 class="title-line">Dúvidas? Veja como funciona!</h2>';
                    echo '<p>Saiba como funcionará cada etapa do projeto, como se cadastrar, como pesquisar e outras informações sobre a Diginet Solidária.</p>';
                    echo '<a href="' . esc_url( home_url() . '/como-funciona/' ) . '">Veja como funciona!</a>';

                echo '</div><!-- /.col -->';
            excellence_close_container();

        echo '</div><!-- /.wrap-links-page-home -->';

    }

    add_action( 'excellence_frontpage', 'excellence_get_section_links_home', 45 );

}

if ( ! function_exists( 'excellence_get_section_search_business' ) ) {

    /**
     * 
     * [9] excellence_get_section_search_business
     * 
     */
    function excellence_get_section_search_business() {
        
        /**
         *
         * Check if the Search & Replace plugin is active
         * 
         */
        if ( class_exists( 'SearchAndFilter' )  ) {

            echo '<div class="wrap-search-business-page-home">';
                excellence_open_container();

                    echo '<div class="col -text-center">';

                        echo '<h2 class="title-line">Encontre negócios perto de você!</h2>';

                        if ( has_nav_menu( 'business-menu' ) ) {

                            echo '<p class="description -text-center">Escolha o que está procurando abaixo ou faça uma pesquisa perto de você.</p>';
                            wp_nav_menu( [ 'theme_location' => 'business-menu' ] );

                        } else {

                            echo '<p class="description -text-center">Pesquise abaixo e encontre produtos e serviços perto de você.</p>';

                        }
                       
                        echo do_shortcode( '[searchandfilter fields="search,atuacao,cidade" search_placeholder="" post_types="perfil" headings="Pesquisar,Área de Atuação,Cidade" all_items_labels="Pesquisar,--,--" submit_label="Pesquisar"]' );
                    
                    echo '</div><!-- /.col -->';

                excellence_close_container();
            echo '</div><!-- /.wrap-search-business-page-home -->';

        }

    }

    add_action( 'excellence_frontpage', 'excellence_get_section_search_business', 40 );

}

/**
 * 
 * [10] excellence_include_templates
 * 
 */
add_filter( 'search_template', 'excellence_include_templates', 99 );

function excellence_include_templates( $template ) {
	
    global $wp_query;  
    
    $new_template = '';

    if ( $wp_query->query_vars['post_types'] == 'perfil' && is_search() ) {
        $new_template = 'search-perfil.php';
  	}
  	
    $plugin_template = EXCELLENCE_PLUGIN_TEMPLATE_DIR . $new_template;
 	
	if ( file_exists( $plugin_template ) ) {
        return $plugin_template;
	}
	
	return $template;
}

//add_action( 'wp_head', 'debugme' );

function debugme() {

    $term = term_exists( '09550250', 'cep' );
    var_dump( $term );

}