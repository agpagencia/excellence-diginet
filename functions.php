<?php

$plugin_dir_path = plugin_dir_path( __FILE__ );


if ( ! function_exists( 'excellence_custom_setup' ) ) {

    function excellence_custom_setup() {

        // Menus
        register_nav_menus(
            [
                'business-menu' => __( 'Menu de Negócios' ),
            ]
        );
        
    }
    add_action( 'init', 'excellence_custom_setup' );

}

if ( ! function_exists( 'excellence_client_enqueue_login_page_scripts' ) ) {

    /**
     * Enqueue Login Page Scripts
     */
    function excellence_client_enqueue_login_page_scripts() {
        wp_enqueue_style( 'excellence-login-page', plugin_dir_url( __FILE__ ) . '/assets/css/excellence-client-login-page.css' );
    }
    add_action( 'login_init', 'excellence_client_enqueue_login_page_scripts' );

}

if ( ! function_exists( 'excellence_client_enqueue_scripts' ) ) {

    /**
     * Enqueue Admin Scripts
     */
    function excellence_client_enqueue_scripts() {
        wp_enqueue_style( 'excellence-admin', plugin_dir_url( __FILE__ ) . '/assets/css/excellence-client-admin.css' );
    }
    add_action( 'admin_enqueue_scripts', 'excellence_client_enqueue_scripts' );

}

if ( ! function_exists( 'excellence_client_scripts' ) ) {

    /**
     * Enqueue Custom Scripts on frontend
     */
    function excellence_client_scripts() {

        wp_enqueue_style( 'excellence-client', plugin_dir_url( __FILE__ ) . 'assets/css/excellence-client.css' );
        wp_enqueue_style( 'select2', plugin_dir_url( __FILE__ ) . 'assets/css/select2.min.css' );
        
        wp_enqueue_script( 'mask', plugin_dir_url( __FILE__ ) . 'assets/js/jquery.mask.min.js', array( 'jquery' ) );
        wp_enqueue_script( 'select2', plugin_dir_url( __FILE__ ) . 'assets/js/select2.min.js', array( 'jquery' ) );
        wp_enqueue_script( 'excellence-client', plugin_dir_url( __FILE__ ) . 'assets/js/excellence-client.js', array( 'select2' ) );

    }
    add_action( 'wp_enqueue_scripts', 'excellence_client_scripts' );

}

if ( ! function_exists( 'excellence_get_the_terms' ) ) {

    function excellence_get_the_terms( $taxonomy ) {
        
        $terms = get_the_terms( get_the_ID(), $taxonomy );
                         
        if ( $terms && ! is_wp_error( $terms ) ) : 
        
            $term_links = array();
        
            foreach ( $terms as $term ) {
                $term_links[] = '<a class="term-' . $term->slug . '" href="' . get_term_link( $term->slug, $taxonomy ) . '">' . $term->name . '</a>';
            }
                                
            $all_terms = join( ' ', $term_links );

            echo '<span class="list-terms">' . $all_terms . '</span>';

        endif;

    }

}

/**
 * Custom Login Page
 */
require_once $plugin_dir_path . '/includes/custom-login-page.php';

/**
 * Cadastro de Empresas/Serviços/Profissionais
 */
require_once $plugin_dir_path . '/includes/business-profile.php';

/**
 * Custom Functions
 */
require_once $plugin_dir_path . '/includes/custom-functions.php';