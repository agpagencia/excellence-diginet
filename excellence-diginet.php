<?php
/**
 * Plugin Name:       Excellence Diginet Solidária
 * Description:       Plugin to customize the client's website using the AGP Agency's Excellence Theme (for WordPres).
 * Plugin URI:        https://gitlab.com/agpagencia/excellence-diginet
 * Version:           0.0.5
 * Author:            Everaldo Matias
 * Author URI:        https://everaldo.dev/
 * Requires at least: 3.0.0
 * Tested up to:      4.4.2
 *
 * @package ExcellenceDiginet
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'EXCELLENCE_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'EXCELLENCE_PLUGIN_TEMPLATE_DIR', EXCELLENCE_PLUGIN_DIR . "templates/" );
define( 'EXCELLENCE_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

/**
 * Main ExcellenceDiginet Class
 *
 * @class ExcellenceDiginet
 * @version	0.0.1
 * @since 0.0.1
 * @package	ExcellenceDiginet
 */
class ExcellenceDiginet {

	/**
	 * Instance of this class.
	 *
	 * @var object
	 */
	protected static $instance = null;

	/**
	 * Plugin directory path
	 *
	 * @var string
	 */
	private $plugin_dir = null;

	/**
	 * Set up the plugin.
	 */
	function __construct() {
		
		$this->plugin_dir = plugin_dir_path( __FILE__ );
		add_action( 'init', array( $this, 'includes' ), 0 );

	}

	/**
	 * Return the plugin instance.
	 */
	public static function init() {
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	public function includes() {

		// Load the functions.php
		require_once $this->plugin_dir . '/functions.php';

	}

} // End Class

/**
* Initialize the plugin actions.
*/
add_action( 'plugins_loaded', array( 'ExcellenceDiginet', 'init' ) );