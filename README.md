# Excellence Diginet

Plugin base para a personalização dos sites desenvolvidos pela AGP Agência com o Excellence Theme.

## Changelog

As alterações registradas nesse changelog são referentes a base do plugin que está repositada no [Gitlab](https://gitlab.com/agpagencia/excellence-diginet)

## [0.0.5] - 2020-06-22

- Add mask on field CPF/CNPJ

## [0.0.4] - 2020-06-22

- Add fields IP, date and hour on the register business.

## [0.0.3] - 2020-06-22

- Add conditional on the print menu business on home.

## [0.0.2] - 2020-04-20

- Add excellence-client.css on the frontend.

## [0.0.1] - 2020-04-17

- First version