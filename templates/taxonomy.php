<?php

/**
 * The template for displaying search results pages
 * @see https://developer.wordpress.org/themes/basics/template-hierarchy/
 */
get_header(); ?>

<main id="site-content" role="main">

    <?php

    global $wp_query;

    $occupation = '';
    if ( isset( $wp_query->query_vars['atuacao'] ) && ! empty( $wp_query->query_vars['atuacao'] ) ) {
        $occupation = $wp_query->query_vars['atuacao'];
    }

    $city = '';
    if ( isset( $wp_query->query_vars['cidade'] ) && ! empty( $wp_query->query_vars['cidade'] ) ) {
        $city = $wp_query->query_vars['cidade'];
    }

    excellence_open_container();

        echo '<div class="col col-search">';
   
            if ( have_posts() ) {

                echo '<h1 class="entry-title">';

                    if ( get_search_query() ) {
                        echo __( 'Resultados para: ', 'excellence' );
                        echo ' <span class="string-search">' . esc_attr( get_search_query() ) . '</span>';
                    } elseif ( $occupation ) {
                        echo ' <span class="string-search">' . excellence_get_term_by_slug( $occupation, 'atuacao' ) . '</span>';                        
                    } elseif ( $city ) {
                        echo ' <span class="string-search">' . excellence_get_term_by_slug( $city, 'cidade' ) . '</span>';                        
                    } else {
                        echo ' <span class="string-search">Negócios Cadastrados</span>';                        
                    }

                echo '</h1><!-- /.entry-title -->';

                while ( have_posts() ) {
                    the_post();

                    $id = get_the_ID();

                    echo '<div class="each-result-search post-id-' . $id . '">';

                        echo '<div class="entry-thumb">';
                            echo '<a href="' . get_the_permalink( $id ) . '">';
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail( 'medium' );
                                } else {
                                    echo '<img src="' . EXCELLENCE_PLUGIN_URL . 'assets/images/default-search.jpg" alt="Logomarca de ' . get_the_title( $id ) . '">';
                                }
                            echo '</a>';    
                        echo '</div><!-- /.entry-thumb -->';
                    
                        echo '<h2 class="entry-title">';
                            echo '<a href="' . get_the_permalink( $id ) . '">';
                                the_title();
                            echo '</a>';
                        echo '</h2><!-- /.entry-title -->';

                        echo '<div class="entry-meta">';
                            excellence_get_the_terms( 'atuacao' );
                        echo '</div><!-- /.entry-meta --> ';
                        
                        echo '<div class="entry-content">';
                            echo apply_filters( 'the_content', excellence_get_excerpt( get_the_content( $id ), '250' ) );
                        echo '</div><!-- /.entry-content -->';

                        echo '<div class="read-more">';
                            echo '<a class="btn" href="' . get_the_permalink( $id ) . '">Leia mais</a>';
                        echo '</div><!-- /.read-more --> ';

                    echo '</div><!-- /.each-result-search -->';

                }

            } else {
                
                echo '<h1 class="entry-title">';
                    echo __( 'Ainda não temos nenhum cadastro para essa pesquisa :(', 'excellence' );
                echo '</h1><!-- /.entry-title -->';

                echo '<div class="entry-content">';

                    echo '<h2>Mas temos algumas sugestões para você:</h2>';
                    echo '<p>Veja todos cadastros em:</p>';
                    
                    if ( $occupation ) {
                        $occupation_link = get_term_by( 'slug', $occupation, 'atuacao' );
                        $occupation_link = get_term_link( $occupation_link->term_id, 'atuacao' );
                        echo '<a href="' . esc_url( $occupation_link ) . '" class="btn">' . excellence_get_term_by_slug( $occupation, 'atuacao' ) . '</a>';
                    }
                    
                    if ( $city ) {
                        $city_link = get_term_by( 'slug', $city, 'cidade' );
                        $city_link = get_term_link( $city_link->term_id, 'cidade' );
                        echo '<a href="' . esc_url( $city_link ) . '" class="btn">' . excellence_get_term_by_slug( $city, 'cidade' ) . '</a>';                        
                    }
                    
                echo '</div><!-- /.entry-content -->';

            }

        echo '</div><!-- /.col -->';

    excellence_close_container(); ?>

</main><!-- /#site-content -->

<?php
excellence_get_section_search_business();
get_footer();