<?php

/**
 * The template for displaying search results pages
 * @see https://developer.wordpress.org/themes/basics/template-hierarchy/
 */
get_header(); ?>

<main id="site-content" role="main">

    <?php
   
    if ( have_posts() ) {

        excellence_open_container();

            echo '<div class="col col-search">';

                echo '<h1 class="entry-title">';
                    echo __( 'Resultados para: ', 'excellence' );
                    echo ' <span class="string-search">' . esc_attr( get_search_query() ) . '</span>';
                echo '</h1><!-- /.entry-title -->';

                while ( have_posts() ) {
                    the_post();

                    $id = get_the_ID();

                    echo '<div class="each-result-search post-id-' . $id . '">';

                        echo '<div class="entry-thumb">';
                            echo '<a href="' . get_the_permalink( $id ) . '">';
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail( 'medium' );
                                } else {
                                    echo '<img src="' . EXCELLENCE_PLUGIN_URL . 'assets/images/default-search.jpg" alt="Logomarca de ' . get_the_title( $id ) . '">';
                                }
                            echo '</a>';    
                        echo '</div><!-- /.entry-thumb -->';
                    
                        echo '<h2 class="entry-title">';
                            echo '<a href="' . get_the_permalink( $id ) . '">';
                                the_title();
                            echo '</a>';
                        echo '</h2><!-- /.entry-title -->';

                        echo '<div class="entry-meta">';
                            excellence_get_the_terms( 'atuacao' );
                        echo '</div><!-- /.entry-meta --> ';
                        
                        echo '<div class="entry-content">';
                            echo apply_filters( 'the_content', excellence_get_excerpt( get_the_content( $id ), '250' ) );
                        echo '</div><!-- /.entry-content -->';

                        echo '<div class="read-more">';
                            echo '<a class="btn" href="' . get_the_permalink( $id ) . '">Leia mais</a>';
                        echo '</div><!-- /.read-more --> ';

                    echo '</div><!-- /.each-result-search -->';

                }

            echo '</div><!-- /.col -->';

        excellence_close_container();

    } ?>

</main><!-- /#site-content -->

<?php
excellence_get_section_search_business();
get_footer();