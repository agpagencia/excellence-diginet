jQuery(document).ready(function() {

    var behavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    options = {
        onKeyPress: function (val, e, field, options) {
            field.mask(behavior.apply({}, arguments), options);
        }
    };

    // MaskS
    jQuery('#cep').mask('00000-000');
    jQuery('#phone').mask('(00) 0000-0000');
    jQuery('#whatsapp').mask(behavior, options);

    function clear_cep() {
        // Limpa valores do formulário de cep.
        jQuery("#rua").val("");
        jQuery("#bairro").val("");
        jQuery("#cidade").val("");
        jQuery("#uf").val("");

        alert("CEP não encontrado.");
    }
    function focus_cep() {
        // Focus on the field #cep
        jQuery("#cep").focus();
    }

    //Quando o campo cep perde o foco.
    jQuery("#cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = jQuery(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                jQuery("#rua").val("...");
                jQuery("#bairro").val("...");
                jQuery("#cidade").val("...");
                jQuery("#uf").val("...");

                //Consulta o webservice viacep.com.br/
                jQuery.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        jQuery("#rua").val(dados.logradouro);
                        jQuery("#bairro").val(dados.bairro);
                        jQuery("#cidade").val(dados.localidade);
                        jQuery("#uf").val(dados.uf);
                        jQuery(".each-line.-hidden").css("display", "block");
                        jQuery(".each-line.number-line #number").focus();
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        //focus_cep();
                        clear_cep();
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                clear_cep();
                focus_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            jQuery(".each-line.-hidden").css("display", "none");
            clear_cep();
        }
    });

    // Activate select2 on fields
    jQuery('.atuacao-field').select2();
    jQuery('#ofatuacao').select2();
    jQuery('#ofcidade').select2();

    /*
    Adiciona máscara em CPF e CNPJ no mesmo campo
    Caso precise você pode mudar o seletor para usar um ID ou class.
    
    Fonte: https://jsfiddle.net/pdd8g4mf/
    */
    var CpfCnpjMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length <= 11 ? '000.000.000-009' : '00.000.000/0000-00';
    },
        cpfCnpjpOptions = {
            onKeyPress: function (val, e, field, options) {
                field.mask(CpfCnpjMaskBehavior.apply({}, arguments), options);
            }
        };

    jQuery(function () {
        jQuery(':input[name=cpf_cnpj]').mask(CpfCnpjMaskBehavior, cpfCnpjpOptions);
    });

});
